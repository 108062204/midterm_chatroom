# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Midterm

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：[xxxx]

## Website Detail Description
一個簡陋的聊天室，能使用自己註冊的帳號或GOOGLE帳號在公共聊天室(類似論壇)和私人聊天室溝通聊天。


# Components Description : 
1. 登入系統 : 在 email login中可以註冊帳號，分別輸入nickname、email和password，其中nickname用來表示聊天室的身分，在登入時不需要輸入(只需要email和password)。而google login則可以直接用google帳號登入。登出的位置與登入系統相同。
2. 創建私人聊天室 : 登入後按下create按鈕，輸入房號後按確定即可創建(房間名稱不可重複，會偵測)。 
3. 回覆 : 在文字欄輸入文字後按下reply按鈕即可(html碼也可以輸入)。

# Other Functions Description : 
1. 邀請 : 點一下要邀請的私人房間，之後按下invite按鈕，輸入名稱(nickname)，接著會確認是否為google帳號(為了不衝突)，確認完後會通知邀請的結果。
2. 退出 : 點一下要退出的私人房間，之後按下drop out按鈕即可退出。(很遺憾，如果要再次進入此房間只能由別人邀請)

