var email;
var pass;
var signin;
var register;
var account;
var login;
var username;
var account_name;
var googlelogin;
var create;
var invite;
var current='public';
var private_list;
var nickname;
var UID='';
var reply;
var comment;
var chat_ref=firebase.database().ref('chat/public');
var first_count=0;
var second_count=0;
var total_post=[];
var total_room=[];
var first_room_count=0;
var second_room_count=0;
/*no animate*/
var str_before_account = "<div class='block'><div class='name text-muted fs-6'>";
var str_after_account = "</div></div>";
var str_before_content = "<div class='block'><div class='texting fs-5'>";
var str_after_content = "</div></div>";
var str_before_account_me = "<div class='block_me'><div class='name_me text-muted fs-6'>";
var str_before_content_me = "<div class='block_me'><div class='texting_me fs-5'>";
var str_before_notification="<div class='notification'><div class='message fs-5'>";
var str_after_notification="</div></div>";

var membername='public';
var with_google=false;
var note_load;

var chat_room_listen;
var list_listen=null;

async function delete_roomlistener(){
    let path='user/'+UID;
    let roomref=firebase.database().ref(path);
    let copy_snapshot;
    let gg=await new Promise((resolve)=>{
        roomref.once('value').then(function(snapshot){
            console.log('ggggg3');
            copy_snapshot=snapshot;
            resolve(1);
        });
    });
    console.log(copy_snapshot);
    let gg2=await new Promise((resolve)=>{
            copy_snapshot.forEach(function(child){
                var data=child.key;
                let ref=firebase.database().ref('chat/'+data);
                ref.off('child_added',note.bind(data));
            });
            resolve(1);
        }).then(()=>{
            console.log('delete_auto');
        });
}
function note(snapshot){
    var data=snapshot.val();
    console.log(snapshot,this);
    if(note_load==true&&data.user_uid!=UID&&data.type==0){
    if (Notification && Notification.permission === "granted") {
          var n = new Notification(this+'\n'+data.user_name+': '+data.text, {tag: 'note'});
      }
      else if (Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {
          if (Notification.permission !== status) {
            Notification.permission = status;
          }
          // If the user said okay
          if (status === "granted") {
                var n = new Notification(this+'\n'+data.user_name+':\n'+data.text, {tag: 'note'});
          }
        });
      }
    }
    else if(note_load==true&&data.user_uid!=UID&&data.type==1){
        if (Notification && Notification.permission === "granted") {
              var n = new Notification(this+':'+data.user_name+' has joined', {tag: 'note'});
          }
          else if (Notification && Notification.permission !== "denied") {
            Notification.requestPermission(function (status) {
              if (Notification.permission !== status) {
                Notification.permission = status;
              }
              // If the user said okay
              if (status === "granted") {
                    var n = new Notification(this+':'+data.user_name+' has joined', {tag: 'note'});
              }
            });
          }
        }
    else if(note_load==true&&data.user_uid!=UID&&data.type==2){
            if (Notification && Notification.permission === "granted") {
                  var n = new Notification(this+':'+data.user_name+' has left', {tag: 'note'});
              }
              else if (Notification && Notification.permission !== "denied") {
                Notification.requestPermission(function (status) {
                  if (Notification.permission !== status) {
                    Notification.permission = status;
                  }
                  // If the user said okay
                  if (status === "granted") {
                        var n = new Notification(this+':'+data.user_name+' has left', {tag: 'note'});
                  }
                });
              }
    }
}
async function onlisten(data) {
    console.log(first_count,second_count);
    second_count += 1;
    if (second_count > first_count) {
        var childData = data.val();
        if(childData.type==1){
        total_post[total_post.length]=str_before_notification+childData.user_name+' join this room.'+str_after_notification;
        }
        else if(childData.type==2)
        {
            total_post[total_post.length]=str_before_notification+childData.user_name+' leave this room.'+str_after_notification;
        }
        else if(childData.user_uid!=UID){
            if(childData.google==true){
                total_post[total_post.length] =str_before_account + childData.user_name+'(google)' + str_after_account + str_before_content + childData.text + str_after_content;
            }
            else{
                total_post[total_post.length] =str_before_account + childData.user_name + str_after_account + str_before_content + childData.text + str_after_content;
            }
        }
        else{
            if(childData.google==true){
                total_post[total_post.length] =str_before_account_me + childData.user_name+'(google)' + str_after_account + str_before_content_me + childData.text + str_after_content;
            }
            else{
                total_post[total_post.length] =str_before_account_me + childData.user_name + str_after_account + str_before_content_me + childData.text + str_after_content;
            }
        }
        document.getElementById('chat_room').innerHTML = total_post.join('');
        var gg1=document.getElementById('chat_room').lastChild;
        var gg2=gg1.lastChild;
        if(gg2.classList.contains('texting_me')){
            gg2.classList.add('right');
            await setTimeout(function(){
                gg2.classList.remove('right');
            },2000);
        }
        else if(gg2.classList.contains('texting')){
            gg2.classList.add('left');
            await setTimeout(function(){
                gg2.classList.remove('left');
            },2000);
        }
        else if(gg2.classList.contains('message')){
            gg2.classList.add('top');
            await setTimeout(function(){
                gg2.classList.remove('top');
            },2000);
        }
        first_count=total_post.length;
    }

}
async function logout_pro()
{
    await delete_roomlistener();
    if(list_listen!=null)
    {
        let path_ref=firebase.database().ref('user/'+UID);
        await path_ref.off('child_added',list_listen);
    }
    firebase.auth().signOut().then(function(){
        logout.removeEventListener('click',logout_pro);
        alert("log out succeed");
    }).catch(function(error){
        var errorMessage = error.message;
        alert("log out failed:"+errorMessage);
    });
}
function encode(text) {
    console.log(text);
    var textArea = document.createElement('textarea');
    textArea.textContent = text;
    let txt=textArea.innerHTML;
    return txt.replace(/\r\n|\r|\n/g,"<br>");
}
async function search(name,auth_google){
    console.log('enter');
    var copy_snapshot;
    var find=false;
    var value_return=null;
    var ref=firebase.database().ref('user_data');
    let gg=await new Promise((resolve)=>{
        ref.once('value').then(function(snapshot){
            console.log('ggggg');
            copy_snapshot=snapshot;
            resolve(1);
        });
    });
    console.log(copy_snapshot);
    let gg2=await new Promise((resolve)=>{
            copy_snapshot.forEach(function(child){
                var data=child.val();
                console.log('foreach loading');
                if(data.user_name==name&&data.google==auth_google)
                {
                    console.log(child.key);
                    find=true;
                    resolve(child.key);
                }
            });
            if(find==false){
            console.log('error');
            resolve(null);
            }
        }).then((value)=>{
            value_return=value;
        });
    return value_return;
}
async function cur(id)
{
    current=id;
    first_count=0;
    second_count=0;
    total_post=[];
    var item_list=document.getElementsByClassName('list_item');
    var item=document.getElementById(id);
    console.log(item);
    for(i=0;i<item_list.length;i=i+1)
    {
        item_list[i].style.backgroundColor="yellowgreen";
    }
    item.style.backgroundColor="yellow";
    let path='chat/'+current;
    document.getElementById('chat_room').innerHTML='';
    await chat_ref.off('child_added',chat_room_listen);
    console.log('off');
    chat_ref=firebase.database().ref(path);
     chat_ref.once('value').then(async function(snapshot){
        await snapshot.forEach(function(childshot) {
            var childData = childshot.val();
            if(childData.type==1){
                total_post[total_post.length]=str_before_notification+childData.user_name+' join this room.'+str_after_notification;
            }
            else if(childData.type==2)
            {
            total_post[total_post.length]=str_before_notification+childData.user_name+' leave this room.'+str_after_notification;
            }
            else if(childData.user_uid!=UID){
                if(childData.google==true){
                    total_post[total_post.length] =str_before_account + childData.user_name+'(google)' + str_after_account + str_before_content + childData.text + str_after_content;
                }
                else{
                    total_post[total_post.length] =str_before_account + childData.user_name + str_after_account + str_before_content + childData.text + str_after_content;
                }
            }
            else{
                if(childData.google==true){
                    total_post[total_post.length] =str_before_account_me + childData.user_name+'(google)' + str_after_account + str_before_content_me + childData.text + str_after_content;
                }
                else{
                    total_post[total_post.length] =str_before_account_me + childData.user_name + str_after_account + str_before_content_me + childData.text + str_after_content;
                }
            }
            first_count+=1;
            console.log('first');
            document.getElementById('chat_room').innerHTML = total_post.join('');
        });
        console.log('on');
         chat_room_listen=chat_ref.on('child_added',onlisten);

    });

}
function init()
{
    list=document.getElementById('private_list');
    nickname=document.getElementById('name_input');
    email=document.getElementById('email_input');
    pass=document.getElementById('password_input');
    signin=document.getElementById('sign_in');
    register=document.getElementById('register');
    account=document.getElementById('account_dropdown');
    login=document.getElementById('login_dropdown');
    username=document.getElementById('username');
    googlelogin=document.getElementById('google_button');
    invite=document.getElementById('invite');
    create=document.getElementById('create');
    delete_btn=document.getElementById('delete');
    reply=document.getElementById('comment_button');
    comment=document.getElementById('comment');

    if (Notification && Notification.permission !== "granted") {
        Notification.requestPermission(function (status) {
          if (Notification.permission !== status) {
            Notification.permission = status;
          }
        });
      }

    signin.addEventListener('click',function()
    {
        firebase.auth().signInWithEmailAndPassword(email.value, pass.value).then( function(user)
        {
            email.value='';
            pass.value='';
            nickname.value='';
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(error);
            alert("login failed:"+errorMessage);
            email.value='';
            pass.value='';
        });
    });

    register.addEventListener('click',async function()
    {
        var search_uid;
        let gg=await search(nickname.value,false);
        search_uid=gg;
        if(search_uid==null){
        firebase.auth().createUserWithEmailAndPassword(email.value,pass.value).then(function(user_token){
            let user=user_token.user;
            let path='user_data/'+user.uid;
            var dataref=firebase.database().ref(path);
            dataref.set({
                email:email.value,
                user_name: nickname.value,
                google: false
            });
            email.value='';
            pass.value='';
            nickname.value='';
            alert('sign up succeed');
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            alert('sign up failed:'+errorMessage);
            email.value='';
            pass.value='';
            nickname.value='';
        });
        }
        else{
            alert('Someone uses this nickname.Please change it to another.');
            email.value='';
            pass.value='';
            nickname.value='';
        }
    });

    googlelogin.addEventListener('click',function()
    {
        var provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithPopup(provider).then(function(result) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            console.log(user);
            let path='user_data/'+user.uid;
            var dataref=firebase.database().ref(path);
            dataref.set({
                email:user.email,
                user_name: user.displayName,
                google: true
            });
          }).catch(function(error) {
              var errorMessage=error.message;
            alert("login failed:"+errorMessage);
          });
    });





    firebase.auth().onAuthStateChanged(async function(user) {
        // Check user login
        note_load=false;
        if (user) {
            let name_ref=firebase.database().ref('user_data/'+user.uid);
            name_ref.once('value').then(function(snapshot)
            {
                let data=snapshot.val();
                account_name=data.user_name;
                username.innerHTML = data.user_name;
            });
            list.innerHTML='';
            UID=user.uid;
            account.style.display='block';
            login.style.display='none';
            create.style.display='block';
            invite.style.display='block';
            delete_btn.style.display='block';
            googlelogin.style.display='none';
            cur('public');
            await room_auto();
            note_load=true;
            logout=document.getElementById('logout');
            logout.addEventListener('click',logout_pro);
        } else {
            username.innerHTML="";
            list.innerHTML='';
            account.style.display='none';
            login.style.display='block';
            create.style.display='none';
            invite.style.display='none';
            delete_btn.style.display='none';
            googlelogin.style.display='block';
            UID=null;
            account_name='';
            cur('public');
        }
    });
    
    reply.addEventListener('click',async function()
    {
        if(UID==null)
        {
            alert('Please login to comment.');
            comment.value='';
        }
        else if(comment.value!='')
        {
            var googletoken=false;
            let path='chat/'+current;
            let postref=firebase.database().ref(path);
            let txt=encode(comment.value);
            let dataref=firebase.database().ref('user_data/'+UID);
            await dataref.once('value').then(function(snapshot){
                console.log(snapshot);
                let data=snapshot.val();
                googletoken=data.google;
                 postref.push({
                    google: googletoken,
                    user_name: account_name,
                    text: txt,
                    user_uid: UID,
                    type:0
                });
            })
            comment.value='';
        }
    });
}

async function room_create(){
    console.log('check');
    roomname=prompt('Please enter the room name.');
    console.log(roomname);
    if(roomname!=''&&roomname!=null&&roomname!='public')
    {
        var samestr=CSS.escape(roomname);
        var  sameroom=await list.querySelector('#'+samestr);
        if(sameroom==null)
        {
        let gg=new Promise(function(resolve,reject)
        {
            total_room[total_room.length]="<div id='"+data +"' class='list_item' onclick=cur('"+data+"')>"+data+"</div>";
            first_room_count+=1;
            list.innerHTML=total_room.join('');
            let ref=firebase.database().ref('chat/'+roomname);
            ref.on('child_added',note.bind(roomname));
            if(document.getElementById( roomname)!=null)
            {
                resolve(1);
            }
            else{
                reject(2);
            }
        });
        gg.then(()=>{
            console.log(document.getElementById( roomname));
        });
        gg.catch(()=>{console.log("gg")});
        let path='user/'+UID+'/'+roomname;
        let roomref=firebase.database().ref(path)
        roomref.set({
            admit: true
        });
        }
        else{
            alert('the room has existed');
        }
    }
    else if(roomname!=null){
        alert('room name is invalid');
    }
}

async function room_invite(){
        if(current!='public')
        {
            var search_uid;
        membername=prompt('Please enter the member nickname.');
        if(membername!=null){
        with_google=confirm('Do you invite a google account?');
        var gg2=await search(membername,with_google);
        search_uid=gg2;
        console.log('complete',search_uid);
        let gg1=await new Promise(function(resolve,reject){
                console.log(search_uid);
                if(search_uid!=null)
                {
                    resolve(1);
                }
                else{
                    reject(2);
                }
        }).then(()=>{
            let userid=search_uid;
            let path='user/'+userid+'/'+current;
            let memberref=firebase.database().ref(path)
            memberref.set({
                admit: true
            });
            let postpath='chat/'+current;
            let postref=firebase.database().ref(postpath);
            postref.push({
                user_name:membername,              
                user_uid:search_uid,
                type:1
            });
            alert('invite successfully');
        }).catch((error) => {
            alert('this member does not exist.');
        });
        }
        }
        else{
            alert('you are in public room.');
        }
    }
async function room_delete(){
    var roomname=current;
    console.log(roomname);
    if(roomname!='public')
    {
        var str=CSS.escape(roomname);
        var  sameroom=await list.querySelector('#'+str);
            let path_chat='chat/'+roomname;
            let ref_chat=firebase.database().ref(path_chat);
            let path_admit='user/'+UID;
            let ref_admit=firebase.database().ref(path_admit);
            await path_chat.push({
                user_name:membername,              
                user_uid:UID,
                type:2
            });
            await ref_admit.once('value').then(async function(snapshot){
                if(snapshot.hasChild(roomname)){
                    await cur('public');
                    ref_chat.off('child_added',note.bind(roomname));
                    list.removeChild(sameroom);
                    ref_admit.child(roomname).remove();
                    alert('leave successfully');
                }
                else{
                    alert('leave failed:you do not have the permission.');
                }
            })
    }
    else {
        alert('you are in public room.');
    }
}
async function room_auto(){
    first_room_count=0;
    second_room_count=0;
    let path='user/'+UID;
    let roomref=firebase.database().ref(path);
    let copy_snapshot;
    let gg=await new Promise((resolve)=>{
        roomref.once('value').then(function(snapshot){
            console.log('ggggg2');
            copy_snapshot=snapshot;
            resolve(1);
        });
    });
    console.log(copy_snapshot);
    let gg2=await new Promise((resolve)=>{
            copy_snapshot.forEach(function(child){
                var data=child.key;
                total_room[total_room.length]="<div id='"+data +"' class='list_item' onclick=cur('"+data+"')>"+data+"</div>";
                let ref=firebase.database().ref('chat/'+data);
                ref.on('child_added',note.bind(data));
                first_room_count+=1;
                list.innerHTML=total_room.join('');
            });
            resolve(1);
        }).then(()=>{
            list_listen=roomref.on('child_added',function(snapshot){
                second_room_count+=1;
                console.log('room',first_room_count,second_room_count);
                if(second_room_count>first_room_count){
                    var data=snapshot.key;
                    total_room[total_room.length]="<div id='"+data +"' class='list_item' onclick=cur('"+data+"')>"+data+"</div>";
                    list.innerHTML=total_room.join('');
                    first_room_count=total_room.length;
                    let ref=firebase.database().ref('chat/'+data);
                    ref.on('child_added',note.bind(data));
                }
            });
            console.log('create_auto');
        });
}


window.onload=function()
{
    init();
};